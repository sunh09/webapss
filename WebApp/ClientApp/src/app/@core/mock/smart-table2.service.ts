import { Injectable } from '@angular/core';
import { SmartTable2Data } from '../data/smart-table2';

@Injectable()
export class SmartTable2Service extends SmartTable2Data {

    data = [{
        userName: 'Kris Marrier ',
        email: 'krismarrier@gmail.com',
        role: 'Administrator',
        company: 'mdo@gmail.com',
        lastActive: 'no',
    }, {
        userName: 'Sage Wieser',
        email: 'sage-wieser@truhlar.uk',
        role: 'user',
        company: 'mdo@gmail.com',
        lastActive: 'no',
    }, {
        userName: 'Leota Dilliard',
        email: 'leota-dilliard@hotmail.com',
        role: 'user',
        company: 'mdo@gmail.com',
        lastActive: 'no',
    }, {
        userName: 'Mitsue Tollner ',
        email: 'tollner-morlong@gmail.com',
        role: 'user',
        company: 'mdo@gmail.com',
        lastActive: 'yes',
    }, {
        userName: 'Simon Morasca',
        email: 'simonm@chapman.com',
        role: 'user',
        company: 'mdo@gmail.com',
        lastActive: 'no',
    }, {
        userName: 'Donette Foller ',
        email: 'foller-donette@in.com',
        role: 'user',
        company: 'mdo@gmail.com',
        lastActive: 'yes',
    }, {
        userName: 'Capla Paprocki ',
        email: 'capla-paprocki@yahoo.com',
        role: 'user',
        company: 'mdo@gmail.com',
        lastActive: 'no',
    }, {
        userName: 'James Venere',
        email: 'ljames-venere@chemel.org',
        role: 'user',
        company: 'mdo@gmail.com',
        lastActive: 'yes',
    }, {
        userName: 'Josephine Darakjy',
        email: 'joesphine-darakjy@chanay.com',
        role: 'user',
        company: 'mdo@gmail.com',
        lastActive: 'no',
    }, {
        userName: 'John Butt ',
        email: 'john-buttbenton@gmail.com',
        role: 'user',
        company: 'mdo@gmail.com',
        lastActive: 'yes',
    },
    {
        userName: 'Josephine Darakjy',
        email: 'joesphine-darakjy@chanay.com',
        role: 'user',
        company: 'mdo@gmail.com',
        lastActive: 'no',
    }, {
        userName: 'John Butt ',
        email: 'john-buttbenton@gmail.com',
        role: 'user',
        company: 'mdo@gmail.com',
        lastActive: 'yes',
    },];

    getData() {
        return this.data;
    }
}
