import { Injectable } from '@angular/core';
import { SmartTable3Data } from '../data/smart-table3';

@Injectable()
export class SmartTable3Service extends SmartTable3Data {

    data = [{
        role: 'Administrator',
        updated: '2019/11/4',
        access: 'Create Role,Delete Role,Create Job,Delete Jobs',
    }, {
        role: 'User',
        updated: '2019/11/4',
        access: 'Read Only Customers,Delete User',
    }, {
        role: 'User',
        updated: '2019/11/6',
        access: 'View Dashboard,Create User',
    },];

    getData() {
        return this.data;
    }
}
