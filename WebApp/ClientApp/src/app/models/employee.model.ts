export class Employee {

    id: number;
    firstName: string;
    lastName: string;
    userName: string;
    email: string;
    age: number;
}