export class SubCategory {
    category: string;
    subCategory: string;
    item: string;
    price: string;
}