export class Users {

    userName: string;
    email: string;
    role: string;
    company: string;
    lastActive: string;
}