import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdministrationComponent } from './administration.component';
import { ManageUsersComponent } from './manage-users/manage-users.component';
import { ManageRolesComponent } from './manage-roles/manage-roles.component';
import { ManageCategoriesComponent } from './manage-categories/manage-categories.component';
import { ManageContactsComponent } from './manage-contacts/manage-contacts.component';
import { ManageFloorplantsComponent } from './manage-floorplants/manage-floorplants.component';
import { CreateUserComponent } from './create-user/create-user.component';

const routes: Routes = [{
  path: '',
  component: AdministrationComponent,
  children: [
    {
      path: 'manage-categories',
      component: ManageCategoriesComponent,
    },
    {
      path: 'manage-users',
      component: ManageUsersComponent,
    },
    {
      path: 'create-user',
      component: CreateUserComponent,
    },
    {
      path: 'manage-roles',
      component: ManageRolesComponent,
    },
    {
      path: 'manage-floorprints',
      component: ManageFloorplantsComponent,
    }
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdministrationRoutingModule { }

export const routedComponents = [
  AdministrationComponent,
  ManageUsersComponent,
  ManageContactsComponent,
  ManageFloorplantsComponent,
];
