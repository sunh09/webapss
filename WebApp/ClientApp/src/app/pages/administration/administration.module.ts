import { NgModule } from '@angular/core';
import { NbCardModule, NbIconModule, NbInputModule, NbCheckboxModule, NbTreeGridModule, NbRadioModule, NbSelectModule, NbButtonModule } from '@nebular/theme';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { ThemeModule } from '../../@theme/theme.module';
import { AdministrationRoutingModule, routedComponents } from './administration-routing.module';
import { ManageRolesComponent } from './manage-roles/manage-roles.component';
import { CreateUserComponent } from './create-user/create-user.component';
import { ManageCategoriesComponent } from './manage-categories/manage-categories.component';

@NgModule({
  imports: [
    NbCardModule,
    NbTreeGridModule,
    NbIconModule,
    NbInputModule,
    NbSelectModule,
    FormsModule,
    ReactiveFormsModule,
    NbRadioModule,
    ThemeModule,
    NbButtonModule,
    NbCheckboxModule,
    AdministrationRoutingModule,
    Ng2SmartTableModule,
  ],
  declarations: [
    ...routedComponents,
    ManageRolesComponent,
    CreateUserComponent,
    ManageCategoriesComponent,
  ],
})
export class AdministrationModule { }
