import { Component, TemplateRef, ViewChild } from '@angular/core';
import { NbWindowService } from '@nebular/theme';
import { from } from 'rxjs';
import { Router, NavigationExtras } from '@angular/router';
@Component({
  selector: 'ngx-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.scss']
})
export class CreateUserComponent {
  selectedOption: string;
  inputAccess: string;
  current_options = [{ id: 1, name: 'name1' }];
  all_options = [{ id: 1, name: 'name1' }, { id: 2, name: 'name2' }];
  @ViewChild('contentTemplate', { static: false }) contentTemplate: TemplateRef<any>;
  constructor(private windowService: NbWindowService, private router: Router) { }

  ngOnInit() {

  }
  refresh(): void {
    window.location.reload();
  }
  goBack() {
    this.router.navigate(['/pages/administration/manage-users']);
  }
  Save() {
    this.router.navigate(['/pages/administration/create-user']);
    // this.location.reload();
    this.refresh();
  }
  saveManager() {
    this.router.navigate(['/pages/administration/manage-users']);
  }
  addRoles() {
    this.inputAccess = this.selectedOption;
  }
  async changeEvent() {
    this.inputAccess = this.selectedOption;
  }
}
