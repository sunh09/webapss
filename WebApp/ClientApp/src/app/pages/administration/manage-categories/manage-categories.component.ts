import { Component, TemplateRef, ViewChild } from '@angular/core';
import { NbDialogService } from '@nebular/theme';
import { from } from 'rxjs';
import { Categories } from '../../../models/categories';
import { SubCategory } from '../../../models/subcategory';
import { LocalDataSource } from 'ng2-smart-table';
import { FormsModule } from '@angular/forms';
import { NgForm } from '@angular/forms';
import { Router, NavigationExtras } from '@angular/router';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'ngx-manage-categories',
  templateUrl: './manage-categories.component.html',
  styleUrls: ['./manage-categories.component.scss']
})
export class ManageCategoriesComponent {
  selectedOption: string;
  inputAccess: string;
  role: string;
  updated: string;
  access: string;
  new_name: string;
  new_description: string;
  new_totalPrice: string;
  new_category: string;
  new_subCategory: string;
  new_item: string;
  new_price: string;
  current_options = [{ id: 1, name: 'name1' }];
  all_options = [{ id: 1, name: 'name1' }, { id: 2, name: 'name2' }];
  @ViewChild('contentTemplate', { static: false }) contentTemplate: TemplateRef<any>;
  source: LocalDataSource = new LocalDataSource();
  constructor(private dialogService: NbDialogService, private router: Router, ) {
    this.source.load(this.categories);
  }
  categories: Categories[] = [
    {
      name: 'Jack  John',
      description: 'He is  very lucky',
      totalPrice: '$50000',
    },
    {
      name: 'Facade',
      description: 'He is very nice',
      totalPrice: '$63334',
    },
    {
      name: 'Servoce Account',
      description: 'The item might not be required',
      totalPrice: '$13000',
    },
  ];

  subcategories: SubCategory[] = [
    {
      category: 'Facade',
      subCategory: 'Facade',
      item: 'Avenue',
      price: '$10000',
    },
    {
      category: 'Site Costs',
      subCategory: 'Hi Slab',
      item: 'The mitem might not be required',
      price: 'Included',
    },
    {
      category: 'Site Costs',
      subCategory: 'Hi Slab',
      item: 'Soild Report..Show More',
      price: '$1000',
    }
  ];
  ngOnInit() {
  }

  settings = {
    actions: {
      add: false,
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      name: {
        title: 'Name',
        type: 'string',
        filter: false,
      },
      description: {
        title: 'Description',
        type: 'string',
        filter: false,
      },
      totalPrice: {
        title: 'Total Price',
        type: 'string',
        filter: false,
      },
    },
    attr: {
      class: 'table table-bordered',
    }
  };
  openWindow(dialog: TemplateRef<any>) {
    this.dialogService.open(dialog, { context: 'this is some additional data passed to dialog' });//.onClose.subscribe(this.new_rol=newRole,);
  }
  addRoles() {
    this.inputAccess = this.selectedOption;
  }
  goBack() {
    this.router.navigate(['/pages/administration/manage-roles']);
  }
  Save() {
    this.categories.push({
      name: this.new_name,
      description: this.new_description,
      totalPrice: this.new_totalPrice,
    });
    this.source.load(this.categories);
  }

  // saveManager() {
  //   this.subcategories.push({
  //     category: this.new_category,
  //     subCategory: this.new_subCategory,
  //     item: this.new_item,
  //     price: this.new_price,
  //   });
  // }
  subSave() {
    this.new_category = '';
    this.new_subCategory = '';
    this.new_item = '';
    this.new_price = '';
    this.new_name = '';
    this.new_description = '';
    this.new_totalPrice = '';

  }

  async changeEvent() {
    this.inputAccess = this.selectedOption;
  }
  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }
}
