import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageFloorplantsComponent } from './manage-floorplants.component';

describe('ManageFloorplantsComponent', () => {
  let component: ManageFloorplantsComponent;
  let fixture: ComponentFixture<ManageFloorplantsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageFloorplantsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageFloorplantsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
