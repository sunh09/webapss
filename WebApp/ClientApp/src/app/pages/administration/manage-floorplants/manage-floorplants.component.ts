import { Component } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';

import { SmartTable1Data } from '../../../@core/data/smart-table1';

@Component({
  selector: 'ngx-manage-floorplants',
  templateUrl: './manage-floorplants.component.html',
  styleUrls: ['./manage-floorplants.component.scss']
})
export class ManageFloorplantsComponent {
  settings = {
    actions: {
      add: false,
      columTitle: "Actions",
      class: "action-colum",
      position: 'right',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      id: {
        title: 'ID',
        type: 'number',
        filter: false,
      },
      name: {
        title: 'Name',
        type: 'string',
        filter: false,
      },
      beds: {
        title: 'Beds',
        type: 'string',
        filter: false,
      },
      bath: {
        title: 'Bath',
        type: 'string',
        filter: false,
      },
      parking: {
        title: 'Parking',
        type: 'string',
        filter: false,
      },
      basePrice: {
        title: 'BasePrice',
        type: 'string',
        filter: false,
      },
      frontage: {
        title: 'Frontage',
        type: 'string',
        filter: false,
      },
      images: {
        title: 'Images',
        type: 'string',
        filter: false,
      },
    },
  };

  source: LocalDataSource = new LocalDataSource();

  constructor(private service: SmartTable1Data) {
    const data = this.service.getData();
    this.source.load(data);
  }

  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }
}

