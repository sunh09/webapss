import { Component, TemplateRef, ViewChild } from '@angular/core';
import { NbDialogService } from '@nebular/theme';
import { from } from 'rxjs';
import { NRole } from '../../../models/role';
import { LocalDataSource } from 'ng2-smart-table';
import { FormsModule } from '@angular/forms';
import { NgForm } from '@angular/forms';
import { Router, NavigationExtras } from '@angular/router';
import { SmartTable3Data } from '../../../@core/data/smart-table3';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
@Component({
  selector: 'ngx-manage-roles',
  templateUrl: './manage-roles.component.html',
  styleUrls: ['./manage-roles.component.scss']
})
export class ManageRolesComponent {
  selectedOption: string;
  inputAccess: string;
  role: string;
  updated: string;
  access: string;
  newRole: string;
  current_options = [{ id: 1, name: 'name1' }];
  all_options = [{ id: 1, name: 'name1' }, { id: 2, name: 'name2' }];
  @ViewChild('contentTemplate', { static: false }) contentTemplate: TemplateRef<any>;
  source: LocalDataSource = new LocalDataSource();
  constructor(private dialogService: NbDialogService, private service: SmartTable3Data, private router: Router, ) {
    const data = this.service.getData();
    this.source.load(data);
  }
  roles: NRole[] = [
    {
      role: 'Administrator',
      updated: '2019/11/4',
      access: 'Create Role,Delete Role,Create Job,Delete Jobs',
    },
    {
      role: 'User',
      updated: '2019/11/4',
      access: 'ReadOnly Customers,Delete User',
    },
    {
      role: 'User',
      updated: '2019/11/6',
      access: 'View Dashboard,Create User',
    }
  ];
  ngOnInit() {
  }

  settings = {
    actions: {
      add: false,
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      role: {
        title: 'Role',
        type: 'string',
        filter: false,
      },
      updated: {
        title: 'Updated',
        type: 'string',
        filter: false,
      },
      access: {
        title: 'Access',
        type: 'string',
        filter: false,
      },
      // lastActive: {
      //   title: 'Last Active',
      //   type: 'html',
      //   valuePrepareFunction: (value) => { return this._sanitizer.bypassSecurityTrustHtml(this.input); },
      //   filter: false
      // },
    },
    attr: {
      class: 'table table-bordered',
    }
  };
  openWindow(dialog: TemplateRef<any>) {
    this.dialogService.open(dialog, { context: 'this is some additional data passed to dialog' });//.onClose.subscribe(this.new_rol=newRole,);
  }
  addRoles() {
    this.inputAccess = this.selectedOption;
  }
  goBack() {
    this.router.navigate(['/pages/administration/manage-roles']);
  }
  Save() {
    this.roles.push({
      role: this.newRole,
      updated: '2019/11/14',
      access: this.selectedOption,
    });
    this.source.load(this.roles);
  }
  async changeEvent() {
    this.inputAccess = this.selectedOption;
  }
  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }
}
