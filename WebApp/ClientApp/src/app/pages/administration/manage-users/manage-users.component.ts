import { Component, TemplateRef, ViewChild } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { NbDialogService } from '@nebular/theme';
import { Router, NavigationExtras } from '@angular/router';
import { SmartTable2Data } from '../../../@core/data/smart-table2';
import { Users } from '../../../models/users';
import { NgForm } from '@angular/forms';
@Component({
  selector: 'ngx-manage-users',
  templateUrl: './manage-users.component.html',
  styleUrls: ['./manage-users.component.scss']
})
export class ManageUsersComponent {
  new_userName: string;
  new_email: string;
  new_role: string;
  new_company: string;
  new_lastActive: boolean;
  @ViewChild('contentTemplate', { static: false }) contentTemplate: TemplateRef<any>;
  users: Users[] = [{
    userName: 'Kris Marrier ',
    email: 'krismarrier@gmail.com',
    role: 'Administrator',
    company: 'mdo@gmail.com',
    lastActive: 'no',
  }, {
    userName: 'Sage Wieser',
    email: 'sage-wieser@truhlar.uk',
    role: 'user',
    company: 'mdo@gmail.com',
    lastActive: 'no',
  }, {
    userName: 'Leota Dilliard',
    email: 'leota-dilliard@hotmail.com',
    role: 'user',
    company: 'mdo@gmail.com',
    lastActive: 'no',
  }, {
    userName: 'Mitsue Tollner ',
    email: 'tollner-morlong@gmail.com',
    role: 'user',
    company: 'mdo@gmail.com',
    lastActive: 'yes',
  }, {
    userName: 'Simon Morasca',
    email: 'simonm@chapman.com',
    role: 'user',
    company: 'mdo@gmail.com',
    lastActive: 'no',
  }, {
    userName: 'Donette Foller ',
    email: 'foller-donette@in.com',
    role: 'user',
    company: 'mdo@gmail.com',
    lastActive: 'yes',
  }, {
    userName: 'Capla Paprocki ',
    email: 'capla-paprocki@yahoo.com',
    role: 'user',
    company: 'mdo@gmail.com',
    lastActive: 'no',
  }, {
    userName: 'James Venere',
    email: 'ljames-venere@chemel.org',
    role: 'user',
    company: 'mdo@gmail.com',
    lastActive: 'yes',
  }, {
    userName: 'Josephine Darakjy',
    email: 'joesphine-darakjy@chanay.com',
    role: 'user',
    company: 'mdo@gmail.com',
    lastActive: 'no',
  }, {
    userName: 'John Butt ',
    email: 'john-buttbenton@gmail.com',
    role: 'user',
    company: 'mdo@gmail.com',
    lastActive: 'yes',
  },
  {
    userName: 'Josephine Darakjy',
    email: 'joesphine-darakjy@chanay.com',
    role: 'user',
    company: 'mdo@gmail.com',
    lastActive: 'no',
  }, {
    userName: 'John Butt ',
    email: 'john-buttbenton@gmail.com',
    role: 'user',
    company: 'mdo@gmail.com',
    lastActive: 'yes',
  },];
  settings = {
    actions: {
      add: false,
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      userName: {
        title: 'User Name',
        type: 'string',
      },
      email: {
        title: 'Email',
        type: 'string',
      },
      role: {
        title: 'Role',
        type: 'string',
      },
      company: {
        title: 'Company',
        type: 'string',
      },
      lastActive: {
        title: 'Last Active',
        type: 'string',
      },

      // lastActive: {
      //   title: 'Last Active',
      //   type: 'html',
      //   valuePrepareFunction: (value) => { return this._sanitizer.bypassSecurityTrustHtml(this.input); },
      //   filter: false
      // },
    },
    attr: {
      class: 'table table-bordered',
    }
  };

  source: LocalDataSource = new LocalDataSource();

  constructor(private service: SmartTable2Data, private router: Router, private dialogService: NbDialogService, ) {
    const data = this.service.getData();
    this.source.load(data);
  }
  CreateUser(dialog: TemplateRef<any>) {
    this.dialogService.open(dialog, { context: 'this is some additional data passed to dialog' });
  }
  saveManager() {
    if (this.new_lastActive) {
      this.users.push({
        userName: this.new_userName,
        email: this.new_email,
        role: this.new_role,
        company: this.new_company,
        lastActive: 'Yes',
      });
    } else {
      this.users.push({
        userName: this.new_userName,
        email: this.new_email,
        role: this.new_role,
        company: this.new_company,
        lastActive: 'Yes',
      });
    }
    this.source.load(this.users);
  }
  Save() {
    this.new_userName = '';
    this.new_email = '';
    this.new_role = '';
    this.new_company = '';
    this.new_lastActive = false;
  }
  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }
}

