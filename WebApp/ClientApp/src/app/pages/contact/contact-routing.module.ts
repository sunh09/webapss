import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ContactComponent } from './contact.component';
import { AllContactsComponent } from './all-contacts/all-contacts.component';
import { CreateContactComponent } from './create-contact/create-contact.component';

const routes: Routes = [{
  path: '',
  component: ContactComponent,
  children: [
    {
      path: 'all-contacts',
      component: AllContactsComponent,
    },
    {
      path: 'create-contact',
      component: CreateContactComponent,
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ContactRoutingModule { }

export const routedComponents = [
  ContactComponent,
  AllContactsComponent,
  CreateContactComponent,
];
