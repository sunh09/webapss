import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import {
  NbCardModule, NbIconModule, NbInputModule, NbListModule, NbTreeGridModule, NbActionsModule, NbButtonModule, NbCheckboxModule, NbDatepickerModule,
  NbRadioModule, NbSelectModule, NbUserModule
} from '@nebular/theme';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { MatPaginator, MatSort } from '@angular/material';
import { ThemeModule } from '../../@theme/theme.module';
import { ContactRoutingModule, routedComponents } from './contact-routing.module';

@NgModule({
  imports: [
    NbCardModule,
    NbTreeGridModule,
    NbIconModule,
    NbInputModule,
    ThemeModule,
    NbActionsModule,
    NbButtonModule,
    NbCardModule,
    FormsModule,
    NbCheckboxModule,
    NbListModule,
    NbDatepickerModule,
    NbIconModule,
    NbInputModule,
    NbRadioModule,
    NbSelectModule,
    NbUserModule,
    ContactRoutingModule,
    Ng2SmartTableModule,
  ],
  declarations: [
    ...routedComponents,

  ],
})
export class ContactModule { }
