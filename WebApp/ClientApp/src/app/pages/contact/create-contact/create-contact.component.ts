import { Component, Input } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router, NavigationExtras } from '@angular/router';
import { NbSortDirection, NbSortRequest, NbTreeGridDataSource, NbTreeGridDataSourceBuilder } from '@nebular/theme';
import { fruits } from './fruits-list';
import * as $ from 'jquery';
interface TreeNode<T> {
  data: T;
  children?: TreeNode<T>[];
  expanded?: boolean;
}

interface FSEntry {
  name: string;
  size: string;
  kind: string;
  items?: number;
}
@Component({
  selector: 'ngx-create-contact',
  templateUrl: './create-contact.component.html',
  styleUrls: ['./create-contact.component.scss']
})
export class CreateContactComponent {
  customColumn = 'name';
  defaultColumns = ['size', 'kind', 'items'];
  allColumns = [this.customColumn, ...this.defaultColumns];
  newUser: string;
  newTitle: string;
  dataSource: NbTreeGridDataSource<FSEntry>;
  selectedItem = '2';
  sortColumn: string;
  sortDirection: NbSortDirection = NbSortDirection.NONE;
  fruits = fruits;

  users: { name: string, title: string }[] = [
    { name: 'Carla Espinosa', title: '   Follow up call today and is scehduled to come in on saturday.' },
    { name: 'Bob Kelso', title: 'Doctor    Follow up call today and is scehduled to come in on saturday. of Medicine' },
    { name: 'Janitor', title: 'Janitor    Follow up call today and is scehduled to come in on saturday.' },
    { name: 'Perry Cox', title: 'Doctor     Follow up call today and is scehduled to come in on saturday.of Medicine' },
    { name: 'Ben Sullivan', title: 'Carpenter    Follow up call today and is scehduled to come in on saturday. and photographer' },
  ];
  constructor(private dataSourceBuilder: NbTreeGridDataSourceBuilder<FSEntry>, private router: Router, ) {
    this.dataSource = this.dataSourceBuilder.create(this.data);
  }
  goBack() {
    this.router.navigate(['/pages/contact/all-contacts']);
  }
  Save() {
    this.router.navigate(['/pages/contact/all-contacts']);
  }

  addNew() {
    this.router.navigate(['/pages/contact/create-contact']);
  }
  updateSort(sortRequest: NbSortRequest): void {
    this.sortColumn = sortRequest.column;
    this.sortDirection = sortRequest.direction;
  }

  getSortDirection(column: string): NbSortDirection {
    if (this.sortColumn === column) {
      return this.sortDirection;
    }
    return NbSortDirection.NONE;
  }
  editNote(index: number) {
    this.newUser = this.users[index].name;
    this.newTitle = this.users[index].title;
  }
  addNote() {
    this.users.push({
      name: this.newUser,
      title: this.newTitle,
    });
  }
  cancelNote() {
    this.newUser = '';
    this.newTitle = '';
  }
  private data: TreeNode<FSEntry>[] = [
    {
      data: { name: 'Projects', size: '1.8 MB', items: 5, kind: 'dir' },
      children: [
        { data: { name: 'project-1.doc', kind: 'doc', size: '240 KB' } },
        { data: { name: 'project-2.doc', kind: 'doc', size: '290 KB' } },
        { data: { name: 'project-3', kind: 'txt', size: '466 KB' } },
        { data: { name: 'project-4.docx', kind: 'docx', size: '900 KB' } },
      ],
    },
    {
      data: { name: 'Reports', kind: 'dir', size: '400 KB', items: 2 },
      children: [
        { data: { name: 'Report 1', kind: 'doc', size: '100 KB' } },
        { data: { name: 'Report 2', kind: 'doc', size: '300 KB' } },
      ],
    },
    {
      data: { name: 'Other', kind: 'dir', size: '109 MB', items: 2 },
      children: [
        { data: { name: 'backup.bkp', kind: 'bkp', size: '107 MB' } },
        { data: { name: 'secret-note.txt', kind: 'txt', size: '2 MB' } },
      ],
    },
  ];

  getShowOn(index: number) {
    const minWithForMultipleColumns = 400;
    const nextColumnStep = 100;
    return minWithForMultipleColumns + (nextColumnStep * index);
  }

  ngAfterViewInit() {
    $("#addtitle").click(function () {
      $("#inputtitle").fadeToggle();
    });

  }
}


@Component({
  selector: 'ngx-fs-icon',
  template: `
    <nb-tree-grid-row-toggle [expanded]="expanded" *ngIf="isDir(); else fileIcon">
    </nb-tree-grid-row-toggle>
    <ng-template #fileIcon>
      <nb-icon icon="file-text-outline"></nb-icon>
    </ng-template>
  `,
})
export class FsIconComponent {
  @Input() kind: string;
  @Input() expanded: boolean;

  isDir(): boolean {
    return this.kind === 'dir';
  }
}
