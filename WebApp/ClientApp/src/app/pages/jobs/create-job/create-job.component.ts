import { Component } from '@angular/core';
import { NbSortDirection, NbSortRequest, NbTreeGridDataSource, NbTreeGridDataSourceBuilder } from '@nebular/theme';
import { Categories } from '../categories_model';
import { Categories_generated } from '../categories_generated';
import { Categories_Total } from '../categories_total_model';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Router, NavigationExtras } from '@angular/router';
interface TreeNode<T> {
  data: T;
  children?: TreeNode<T>[];
  expanded?: boolean;
}

interface FSEntry {
  'Category': string;
  'Categorys': string;
  'Sub Category': string;
  'Item': string;
  'Price': string;
}
@Component({
  selector: 'ngx-create-job',
  templateUrl: './create-job.component.html',
  styleUrls: ['./create-job.component.scss']
})
export class CreateJobComponent {
  category: string;
  subCategory: string;
  item: string;
  price: string;
  total: string;
  name: string;
  addFlag: false;
  generatedOn: string;
  new_category: string;
  new_subCategory: string;
  new_item: string;
  new_price: string;
  addGeneratedFlag: false;
  new_name: string;
  new_generatedOn: string;
  categories: Categories[] = [
    {
      category: 'Facade',
      subCategory: 'Facade',
      item: 'Avenue',
      price: '$11000',
    },
    {
      category: 'Site Costs',
      subCategory: 'Hi Slab',
      item: 'The mitem might not be required',
      price: 'Included',
    },
    {
      category: 'Site Costs',
      subCategory: 'Hi Slab',
      item: 'Soild Report..Show More',
      price: '$1000',
    }
  ];
  categories_total: Categories_Total[] = [
    {
      category: 'Facade',
      total: '$1000',
    },
    {
      category: 'Site Costs',
      total: '$1600',
    },
    {
      category: 'Site Costs',
      total: '$2500',
    }
  ];
  deleteCategorie(index) {
    this.categories.splice(index, 1);
  }
  addCategory() {
    this.categories.push({
      category: this.new_category,
      subCategory: this.new_subCategory,
      item: this.new_item,
      price: this.new_price,
    });
  }
  addGenereted() {
    this.categories_generated.push({
      name: this.new_name,
      generatedOn: this.new_generatedOn,
    });
  }

  gotoBack() {
    this.router.navigate(['/pages/jobs/all-jobs']);
  }
  gotoAllContacts() {
    this.router.navigate(['/pages/contact/all-contacts']);
  }
  categories_generated: Categories_generated[] = [
    {
      name: 'Temder_2019_11_5',
      generatedOn: '05 Nov 2019',
    },
    {
      name: 'Temder_2019_11_5',
      generatedOn: '05 Nov 2019',
    }
  ];
  customColumn = 'Category';
  defaultColumns = ['Categorys', 'Sub Category', 'Item', 'Price'];
  allColumns = [this.customColumn, ...this.defaultColumns];

  dataSource: NbTreeGridDataSource<FSEntry>;

  sortColumn: string;
  sortDirection: NbSortDirection = NbSortDirection.NONE;

  constructor(private dataSourceBuilder: NbTreeGridDataSourceBuilder<FSEntry>, private router: Router, ) {
    this.dataSource = this.dataSourceBuilder.create(this.data);
    this.new_name = 'Temder_2019_11_13';
    this.new_generatedOn = '13 Nov 2019';
  }

  private data: TreeNode<FSEntry>[] = [
    {
      data: {
        'Category': 'Facade1', 'Categorys': 'Facade', 'Sub Category': 'Facade', Item: 'Avenue', Price: '$1000',
      },
    },
    {
      data: {
        'Category': 'Site Costs', 'Categorys': 'Facade1', 'Sub Category': 'Hi Slab', Item: 'The item might not be required', Price: 'Include'
      },
    },
    {
      data: {
        'Category': 'Site Costs1', 'Categorys': 'Facade', 'Sub Category': 'Hi Slab', Item: 'Soil Report... ... show More', Price: '$1000'
      },
    },
  ];
}