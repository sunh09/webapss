import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeneratedFilesComponent } from './generated-files.component';

describe('GeneratedFilesComponent', () => {
  let component: GeneratedFilesComponent;
  let fixture: ComponentFixture<GeneratedFilesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeneratedFilesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeneratedFilesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
