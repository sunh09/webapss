import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InProgressComponent } from './in-progress.component';

describe('AllContactsComponent', () => {
  let component: InProgressComponent;
  let fixture: ComponentFixture<InProgressComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [InProgressComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InProgressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
