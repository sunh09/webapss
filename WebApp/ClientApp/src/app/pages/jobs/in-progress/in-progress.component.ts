import { Component, Input } from '@angular/core';
import { NbSortDirection, NbSortRequest, NbTreeGridDataSource, NbTreeGridDataSourceBuilder } from '@nebular/theme';
import { Router, NavigationExtras } from '@angular/router';
interface TreeNode<T> {
  data: T;
  children?: TreeNode<T>[];
  expanded?: boolean;
}

interface FSEntry {
  'Job Number': string;
  'Address': string;
  'Customer Name': string;
  'Age': string;
  'Start Date': string;
  'Price': string;
}

@Component({
  selector: 'ngx-in-progress',
  templateUrl: './in-progress.component.html',
  styleUrls: ['./in-progress.component.scss']
})
export class InProgressComponent {

  customColumn = 'Job Number';
  defaultColumns = ['Address', 'Customer Name', 'Age', 'Start Date', 'Price'];
  allColumns = [this.customColumn, ...this.defaultColumns];

  dataSource: NbTreeGridDataSource<FSEntry>;

  sortColumn: string;
  sortDirection: NbSortDirection = NbSortDirection.NONE;

  constructor(private dataSourceBuilder: NbTreeGridDataSourceBuilder<FSEntry>, private router: Router, ) {
    this.dataSource = this.dataSourceBuilder.create(this.data);
  }

  updateSort(sortRequest: NbSortRequest): void {
    this.sortColumn = sortRequest.column;
    this.sortDirection = sortRequest.direction;
  }

  getSortDirection(column: string): NbSortDirection {
    if (this.sortColumn === column) {
      return this.sortDirection;
    }
    return NbSortDirection.NONE;
  }
  openWindow() {
    this.router.navigate(['/pages/jobs/create-job']);
  }
  private data: TreeNode<FSEntry>[] = [
    {
      data: {
        'Job Number': 'Airi Satou', 'Address': 'Accountant', 'Customer Name': 'Tokyo', 'Age': '33', 'Start Date': '2008//11/28', 'Price': '$162,700'
      },
    },
    {
      data: {
        'Job Number': 'Angelica Ramos', 'Address': 'Chief Execute Officer(CEO)', 'Customer Name': 'London', 'Age': '47', 'Start Date': '2009/10/09', 'Price': '$120,000'
      },
    },
    {
      data: {
        'Job Number': 'Ashton Cox', 'Address': 'Junior Technical Author', 'Customer Name': 'San Francisco', 'Age': '66', 'Start Date': '2009/01/12', 'Price': '$86,000'
      },
    },
    {
      data: {
        'Job Number': 'Bradley Greer', 'Address': 'Software Engineer', 'Customer Name': 'London', 'Age': '41', 'Start Date': '2012/10/13', 'Price': '$132,000'
      },
    }
  ];

  getShowOn(index: number) {
    const minWithForMultipleColumns = 400;
    const nextColumnStep = 100;
    return minWithForMultipleColumns + (nextColumnStep * index);
  }
}

@Component({
  selector: 'ngx-fs-icon',
  template: `
    <nb-tree-grid-row-toggle [expanded]="expanded" *ngIf="isDir(); else fileIcon">
    </nb-tree-grid-row-toggle>
    <ng-template #fileIcon>
      <nb-icon icon="file-text-outline"></nb-icon>
    </ng-template>
  `,
})
export class FsIconComponent {
  @Input() kind: string;
  @Input() expanded: boolean;

  isDir(): boolean {
    return this.kind === 'dir';
  }
}

