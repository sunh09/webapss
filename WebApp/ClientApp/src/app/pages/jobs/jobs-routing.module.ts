import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { JobsComponent } from './jobs.component';
import { InProgressComponent } from './in-progress/in-progress.component';
import { CompletedComponent } from './completed/completed.component';
import { CreateJobComponent } from './create-job/create-job.component';

const routes: Routes = [{
  path: '',
  component: JobsComponent,
  children: [
    {
      path: 'all-jobs',
      component: InProgressComponent,
    },
    {
      path: 'completed',
      component: CompletedComponent,
    },
    {
      path: 'create-job',
      component: CreateJobComponent,
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class JobsRoutingModule { }

export const routedComponents = [
  JobsComponent,
  InProgressComponent,
  CompletedComponent,
  CreateJobComponent,
];
