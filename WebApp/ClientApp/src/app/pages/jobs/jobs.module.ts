import { NgModule } from '@angular/core';
import { NbCardModule, NbIconModule, NbInputModule, NbTabsetModule, NbButtonModule, NbTreeGridModule, NbSearchModule, NbSelectModule } from '@nebular/theme';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { FormsModule } from '@angular/forms';
import { ThemeModule } from '../../@theme/theme.module';
import { JobsRoutingModule, routedComponents } from './jobs-routing.module';
import { FsIconComponent } from './in-progress/in-progress.component';
import { CreateJobComponent } from './create-job/create-job.component';
import { TenderTableComponent } from './tender-table/tender-table.component';
import { CategoryTotalComponent } from './category-total/category-total.component';
import { GeneratedFilesComponent } from './generated-files/generated-files.component';
@NgModule({
  imports: [
    NbCardModule,
    NbTreeGridModule,
    NbIconModule,
    NbInputModule,
    NbButtonModule,
    NbSelectModule,
    NbSearchModule,
    ThemeModule,
    FormsModule,
    NbTabsetModule,
    JobsRoutingModule,
    Ng2SmartTableModule,
  ],
  declarations: [
    ...routedComponents,
    FsIconComponent,
    CreateJobComponent,
    TenderTableComponent,
    CategoryTotalComponent,
    GeneratedFilesComponent
  ],
})
export class JobsModule { }
