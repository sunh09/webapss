import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'Dashboard',
    icon: 'shopping-cart-outline',
    link: '/pages/dashboard',
    home: true,
  },
  {
    title: 'FEATURES',
    group: true,
  },
  {
    title: 'Administration',
    icon: 'browser-outline',
    children: [
      {
        title: 'Manage Users',
        link: '/pages/administration/manage-users',
      },
      {
        title: 'Manage Roles',
        link: '/pages/administration/manage-roles',
      },
      {
        title: 'Manage Categories',
        link: '/pages/administration/manage-categories',
      },
      // {
      //   title: 'Manage Floorplants',
      //   link: '/pages/administration/manage-floorprints',
      // },
    ],
  },
  {
    title: 'Jobs',
    icon: 'layout-outline',
    children: [
      {
        title: 'All Jobs',
        link: '/pages/jobs/all-jobs',
      },
      {
        title: 'Completed',
        link: '/pages/jobs/completed',
      },
      // {
      //   title: 'Create Job',
      //   link: '/pages/jobs/create-job',
      // },
    ]
  },
  {
    title: 'Contacts',
    icon: 'edit-2-outline',
    children: [
      {
        title: 'AllContacts',
        link: '/pages/contact/all-contacts',
      },
      // {
      //   title: 'create-contact',
      //   link: '/pages/contact/create-contact',
      // },
    ]
  },
];
